function createNewUser(firstName, lastName, birthday) {
    const newUser = {
        firstName: prompt("Enter your name"),
        lastName: prompt("Enter your last name"),
        birthday: prompt("Enter your date of birth in the format dd.mm.yyyy"),
        getAge: function() {
            let dateArray = this.birthday.split('.');
            let birthYear = dateArray[2],
                birthMonth = dateArray[1],
                birthDate = dateArray[0];
            let today = new Date();
            let currentYear = today.getFullYear(),
                currentMonth = today.getMonth() + 1,
                currentDate = today.getDate();
            let age = currentYear - birthYear;
            if (currentMonth < birthMonth) {
                age = age - 1;
            } else if (currentMonth == birthMonth && currentDate < birthDate) {
                age = age - 1;
            }
            return age;
        },
        getLogin: function() {
            return this.firstName[0].toLocaleLowerCase() + this.lastName.toLowerCase();
        },
        getPassword: function() {
            let dateArray = this.birthday.split('.');
            let birthYear = dateArray[2];
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthYear;
        }

    }
    return newUser;
}
const newUser = createNewUser();
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());